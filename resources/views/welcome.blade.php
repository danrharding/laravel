

<!DOCTYPE html>
<html lang="en">
<head>@include('includes.head')</head>
<body>

<div class="container">
    <div class="jumbotron text-center">
        <h1>Welcome</h1>
    </div>
    @include('includes.header')
    <div class="row text-center">
        <div class="col-sm-12"><h2><a href="{{ route('rota-slot-staff.index') }}">Click here</a> to view the staff Rota</h2></div>
        <div class="col-sm-12">@include('includes.footer')</div>
    </div>
</div>

</body>
</html>
