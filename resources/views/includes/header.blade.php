<div class="navbar navbar-default">
    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav">
                <li @if(empty(Route::currentRouteName()))
                        class="active"
                    @endif><a href="{{ url('/') }}">Home</a></li>
                <li @if(Route::currentRouteName() === 'rota-slot-staff.index')
                        class="active"
                    @endif><a href="{{ route('rota-slot-staff.index') }}">Staff Rota</a></li>
            </ul>
        </div>
    </div>
</div>