@extends('layouts.default')

@section('content')
    <link rel="stylesheet" href="{{ URL::asset('css/rota-slot-staff.css') }}">

    <h2>Staff Rota</h2>

    @if ( !$dayArray )
        You have no projects
    @else
        <table class="rota-table table table-striped">
            <thead>
                <td>Staff id</td>
                <td>Monday</td>
                <td>Tuesday</td>
                <td>Wednesday</td>
                <td>Thursday</td>
                <td>Friday</td>
                <td>Saturday</td>
                <td>Sunday</td>
            </thead>
            <tbody>
                @foreach( $dayArray as $staffId => $rota )
                    <tr>
                        <td>{{ $staffArray[$staffId] }}</td>
                        @foreach( $rota as $id => $days )
                            <td class="current-day">
                                @if( $days['starttime'] && $days['endtime']  )
                                    <div class="hover-info down-arrow">
                                        <b class="staff-title">{{ $staffArray[$staffId] }}</b><br />
                                        <b>Working hours:</b> {{ $days['workhours'] }}<br />
                                        <b>Start time:</b> {{ $days['starttime'] }}<br />
                                        <b>End time:</b> {{ $days['endtime'] }}<br />
                                    </div>
                                @endif
                                {{ $days['starttime'] }} - {{ $days['endtime'] }}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div id="status"></div>
    @endif
@endsection


{{--{"id":44317,
"rotaid":332,
"daynumber":0,
"staffid":0,
"slottype":"shift",
"starttime":"19:00:00",
"endtime":"03:00:00",
"workhours":8,
"premiumminutes":0,
"roletypeid":14,
"othervalue":null,
"seniorcashierminutes":0,
"splitshifttimes":"",
"created_at":"-0001-11-30 00:00:00",
"updated_at":"-0001-11-30 00:00:00"}--}}