<!DOCTYPE html>
<html lang="en">
<head>@include('includes.head')</head>
<body>

<div id="main-container" class="container">
    <div class="jumbotron text-center">
        <h1>Staff Rota</h1>
    </div>
    @include('includes.header')
    <div class="row">
        <div class="col-sm-12">@yield('content')</div>
        <div class="col-sm-12 text-center">@include('includes.footer')</div>
    </div>
</div>

</body>
</html>
