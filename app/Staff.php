<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    const STAFF_TABLE = 'staff';

    protected $table = self::STAFF_TABLE;
}
