<?php

Route::get('/', function () {
    return view('welcome');
});

Route::model('rota-slot-staff', 'RotaSlotStaff');

Route::resource('rota-slot-staff', 'RotaSlotStaffController');