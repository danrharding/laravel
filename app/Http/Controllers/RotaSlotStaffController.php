<?php

namespace App\Http\Controllers;

use App\RotaSlotStaff;
use App\Staff;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use view;

class RotaSlotStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dayArray = [];

        $staffArray = $this->fetchStaffArray();

        $rotaSlotStaff = DB::table(RotaSlotStaff::ROTA_TABLE)
            ->where('slottype', '=', 'shift')
            ->orderBy('staffid', 'asc')
            ->orderBy('daynumber', 'asc')
            ->having('staffid', '>', 1)
            ->get();

        foreach ($rotaSlotStaff as $day) {
            if(!isset($dayArray[$day->staffid][0])) {
                $dayArray[$day->staffid] = [
                    0, 1, 2, 3, 4, 5, 6,
                ];
            }

            $staffName = DB::table('staff')->where('id', $day->staffid)->pluck('name');

            $dayArray[$day->staffid][$day->daynumber] = (array) $day;
        }

        return view('rota-slot-staff.index', compact(
            'dayArray',
            'staffArray'
        ));
    }

    public function fetchStaffArray()
    {
        $staffArray = [];

        $staff = DB::table(Staff::STAFF_TABLE)->get();

        foreach ($staff as $member) {
            $staffArray[$member->id] = $member->name;
        }

        return $staffArray;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(RotaSlotStaff $rotaSlotStaff)
    {
        return view('projects.show', compact('rota-slot-staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}