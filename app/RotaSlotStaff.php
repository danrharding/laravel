<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RotaSlotStaff extends Model
{
    const ROTA_TABLE = 'rota_slot_staff';

    protected $table = self::ROTA_TABLE;
}
