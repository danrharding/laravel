App.RotaSlotStaff = function() {

    var currentY = {
        top:    'auto',
        bottom: 'calc(100% + 10px)'
    };

    var modifiedY = {
        bottom: 'auto',
        top:    'calc(100% + 10px)'
    };

    var modifiedX = {
        right: 'auto',
        left:  '0px'
    };

    var currentX = {
        left:  'auto',
        right: '0px'
    };

    function position(pos, type) {
        (pos > 50) ? $('.hover-info').css((type === 'X') ? currentX : currentY) :
                     $('.hover-info').css((type === 'X') ? modifiedX : modifiedY);
    }

    function rotaInfoBox() {
        $(document).mousemove(function(e){
            var calcY = Math.round((e.pageY + window.pageYOffset) / 100) + '0';
            var calcX = Math.round((e.pageX / window.innerWidth) * 100);

            position(calcY, 'Y');
            position(calcX, 'X');

            (calcY > 50) ? $('.hover-info').removeClass('up-arrow').addClass('down-arrow') :
                           $('.hover-info').removeClass('down-arrow').addClass('up-arrow');
        });
    }

    function init() {
        rotaInfoBox();
    }

    return {
        init: init
    };

}();

$(document).ready(App.RotaSlotStaff.init);