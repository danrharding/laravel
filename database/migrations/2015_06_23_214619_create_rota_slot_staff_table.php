<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRotaSlotStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rota_slot_staff', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('rotaid')->unsigned()->default(0);
            $table->tinyInteger('daynumber')->default(NULL);
            $table->integer('staffid')->default(NULL);
            $table->foreign('staffid')->references('id')->on('staff');
            $table->string('slottype')->default(NULL);
            $table->time('starttime')->nullable()->default(NULL);
            $table->time('endtime')->nullable()->default(NULL);
            $table->float('workhours')->default(4, 2);
            $table->integer('premiumminutes')->nullable()->default(NULL);
            $table->integer('roletypeid')->nullable()->default(NULL);
            $table->integer('othervalue')->nullable()->default(NULL);
            $table->integer('seniorcashierminutes')->nullable()->default(NULL);
            $table->string('splitshifttimes')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rota_slot_staff');
    }
}
