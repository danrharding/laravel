<?php

use DB as Database;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('staff')->delete();

        $tasks = array(
            ['id' => 3, 'name' => 'Mallie Uriarte'],
            ['id' => 4, 'name' => 'Daniel Migliore'],
            ['id' => 23, 'name' => 'Alona Beaudette'],
            ['id' => 24, 'name' => 'Jaqueline Allgeier'],
            ['id' => 32, 'name' => 'Teresa Swiney'],
            ['id' => 53, 'name' => 'Vanita Siniard'],
            ['id' => 54, 'name' => 'Yoko Tell'],
            ['id' => 55, 'name' => 'Guillermo Rameriz'],
            ['id' => 56, 'name' => 'Joana Carlson'],
            ['id' => 59, 'name' => 'Jackelyn Graybill'],
        );

        //// Uncomment the below to run the seeder
        DB::table('staff')->insert($tasks);
    }
}
